import Vue from 'vue'
import Router from 'vue-router'
import TodoRoutes from './todo-routes'

Vue.use(Router)

const router = new Router({
  routes: [
    { ...TodoRoutes }
  ]
})

export default router
