import Vue from 'vue'
import Router from 'vue-router'
import TodoEntry from '@/components/pages/todoEntry/TodoEntry'
import Index from '@/components/pages/todoEntry/Index'
import Member from '@/components/pages/todoEntry/Member'

Vue.use(Router)

export default {
  path: '/',
  component: TodoEntry,
  children: [
    {
      path: '',
      component: Index
    },
    {
      path: '/',
      component: Index
    },
    {
      path: '/members/:id',
      component: Member
    }
  ]
}
