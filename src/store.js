// プリケーション全体として必要な情報
const state = {
    todo: '',
  newTodo: '',
  elements: []
}

const action = {
    addTodo: function () {
      let inputValue = state.newTodo
      // ''の場合は追加しない
      if (!inputValue) {
        return
      }
      state.elements.push({
        todo: inputValue,
        isChecked: false,
        editing: false
      })
      this.newTodo = '' // textboxの中を初期化
    },
    deleteTodo: function (target) {
      this.elements = this.elements.filter((item) => {
        return item.todo !== target.todo
      })
    },
    editTodo: function (target) {
      this.beforeCache = target.todo
      target.editing = true
    },
    doneEdit: function (target) {
      target.todo = target.todo.trim()
      // 修正後''の場合は削除
      if (target.todo === '') {
       this.deleteTodo(target)
     　}
      target.editing = false
    },
    cansellEdit: function (target) {
      target.todo = this.beforeCache
      target.editing = false
    }
}

// 同じ処理をutilの中に関数として定義 必要なものを返すだけにする
const util = {
  isEmpty (todo) {
    return todo === ''
  }
}
// ほかの箇所で使えるようにexport
export default = {
  state, action
}