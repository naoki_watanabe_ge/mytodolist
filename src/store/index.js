import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  todos: [],
  dones: []
}

const actions = {
  show ({commit}) {
    commit('show')
  },
  close ({commit}) {
    commit('close')
  },
  increment ({commit}) {
    commit('increment')
  },
  decrement ({commit}) {
    commit('decrement')
  }
}

const mutations = {
  show (state) {
    state.isShow = true
  },
  close (state) {
    state.isShow = false
  },
  increment (state) {
    state.count += 1
  },
  decrement (state) {
    state.count -= 1
  }
}

const getters = {
  getCount (state) {
    return state.count
  },
  getModalState (state) {
    return state.isShow
  }
}

export default new Vuex.Store({
  namespaced: true,
  state,
  actions,
  mutations,
  getters
})
