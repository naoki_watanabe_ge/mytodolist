import Vue from 'vue'
import Vuex from 'vuex'
import Count from './modules/Count'

Vue.use(Vuex)

const state = {
}

const modules = {
  Count
}

const store = new Vuex.Store({
  state,
  modules
})

export default store
